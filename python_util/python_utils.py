# coding:utf-8

from urllib.parse import unquote_plus


def get_obs_obj_info_from_event(event):
    if 's3' in event:
        s3 = event['s3']
        return event["eventRegion"], s3['bucket']['name'], unquote_plus(s3['object']['key'])
    else:
        obs_info = event['obs']
        return event["eventRegion"], obs_info['bucket']['name'], \
               unquote_plus(obs_info['object']['key']), event["dynamic_source"]
