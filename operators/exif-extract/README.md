# 算子说明
本算子读取图片头部 64k 内容，解析 EXIF 信息并返回，也可以根据参数设置将 EXIF 信息以 Json 格式存储至目标路径

## 1. 算子信息

- Runtime: Python 3.6
- 内存：128M
- 建议执行超时时间：3s
- 可配置参数
  - target_bucket: 结果 json 文件输出桶
  - target_prefix: 结果 json 文件输出前缀
- 依赖包
  - 公共依赖包: obs==3.21.8
  - 私有依赖包：exifread==3.0.0
- 权限：
    - obs:object:GetObject
    - obs:object:PutObject
    - obs:object:ModifyObjectMetaData
- 性能参考: 
  - 冷启动：800ms~2000ms
  - 非冷启动：30~200ms


## 2. 适用场景
1. 上传后将 EXIF 信息存储至数据库，方便在数据库中根据 EXIF 信息检索图片
## 3. 工作流模板举例
待补充