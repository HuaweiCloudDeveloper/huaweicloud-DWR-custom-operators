# coding:utf-8
import json

from obs import ObsClient, GetObjectHeader

import exifread
from exifread.classes import IfdTag
from python_util.python_utils import get_obs_obj_info_from_event


def getExifInfo(file_path):
    f = open(file_path, 'rb')
    tags = exifread.process_file(f)
    tag_dict = {k: v.printable for k, v in tags.items() if isinstance(v, IfdTag)}
    return tag_dict


def handler(event, context):
    region_id, bucket_name, object_name, dynamic_source = get_obs_obj_info_from_event(event.get("Records", None)[0])
    context.getLogger().info(f"bucket name: {bucket_name}, object key: {object_name}")
    server = 'obs.' + region_id + '.myhuaweicloud.com'
    client = ObsClient(context.getAccessKey(), context.getSecretKey(),
                       server=server,
                       ssl_verify=False)
    # 获取图片头 64k 至本地（EXIF 最大 64k）
    headers = GetObjectHeader()
    # 指定开始和结束范围
    headers.range = '0-65537'
    resp = client.getObject(bucket_name, object_name, loadStreamInMemory=True, headers=headers)
    if resp.status < 300:
        with open(f"/tmp/{object_name}", "wb") as f:
            f.write(resp.body.buffer)
    else:
        context.getLogger().info(f"GetObject failed, status is {resp.status}")
        return {}

    # 处理文件
    with open(f"/tmp/{object_name}", "rb") as f:
        tags = exifread.process_file(f)

    exif_dict = {k: v.printable for k, v in tags.items() if isinstance(v, IfdTag)}

    # 将转换后的文件上传到新的obs桶中
    if dynamic_source['target_bucket'] != "":
        if dynamic_source['target_prefix'] != "":
            client.putObject(dynamic_source['target_bucket'], f"{dynamic_source['target_prefix']}/{object_name}_exif.json", json.dumps(exif_dict))
        else:
            client.putObject(dynamic_source['target_bucket'],
                             f"{object_name}_exif.json", json.dumps(exif_dict))

    return {"tasks": [json.dumps(exif_dict)]}
