# 算子说明
使用SQL处理obs中的csv对象。
将该工具部署为自定义算子，使用该jar包中SelectClient调用该算子，该算子会执行SelectClient发送过来的sql语句，从客户端指定的对象中读取数据，并使用sql进行过滤，将过滤结果返回至客户端。

## 1. 算子信息

- Runtime: Java 8
- 内存：512M
- 建议执行超时时间：30s
- 可配置参数
  - region: 函数所在region
- 依赖包
  - 私有依赖包: RunTime-1.1.3
  - 公共依赖包: commons-codec-1.9
  - 公共依赖包: commons-csv-1.9.0
  - 公共依赖包: commons-logging-1.2
  - 公共依赖包: csvjdbc-1.0-36
  - 公共依赖包: esdk-obs-java-optimised-3.22.10.1
  - 公共依赖包: gson-2.8.5
  - 公共依赖包: httpasyncclient-4.1.2
  - 公共依赖包: httpclient-4.5.3
  - 公共依赖包: httpcore-4.4.4
  - 公共依赖包: httpcore-nio-4.4.4
  - 公共依赖包: jackson-annotations-2.13.2
  - 公共依赖包: jackson-core-2.13.2
  - 公共依赖包: jackson-databind-2.13.2.2
  - 公共依赖包: java-xmlbuilder-1.3
  - 公共依赖包: jna-4.1.0
  - 公共依赖包: log4j-api-2.5
  - 公共依赖包: log4j-core-2.5
  - 公共依赖包: okhttp-3.14.9
  - 公共依赖包: okio-1.17.5
- 权限：
    - obs:object:GetObject
    - obs:object:PutObject
    - obs:object:ModifyObjectMetaData
- 性能参考（过滤10M数据）: 
  - 冷启动：1000ms~3000ms
  - 非冷启动：30~250ms


## 2. 适用场景
需要使用sql查询存储在obs中的csv文件。
## 3. 工作流模板举例
待补充