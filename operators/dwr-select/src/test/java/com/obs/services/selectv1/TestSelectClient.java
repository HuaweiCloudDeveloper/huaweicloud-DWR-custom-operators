package com.obs.services.selectv1;

import com.obs.services.selectv1.impl.dwr.DwrRequestParam;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TestSelectClient {
    public static void main(String[] args) throws IOException {
        String ak = args[0];
        String sk = args[1];
        String endpoint = args[2];
        String bucket = args[3];
        String key = args[4];
        String workFlow = args[5];

        SelectClient.SelectConfig conf = new SelectClient.SelectConfig();
        conf.mode = SelectClient.SelectMode.DWR;
        conf.ak = ak;
        conf.sk = sk;
        conf.endpoint = endpoint;
        conf.dwrWorkflowName = "obs-select";
//        conf.dwrDstBucket = "zjntest";
        conf.dwrDstDir = ".dwr-select";
        conf.timeout = 10;
        conf.dwrResponseMode = DwrRequestParam.DwrResponseMode.FILE_STREAM;

        long time = System.currentTimeMillis();
        SelectClient client = new SelectClient(conf);
        SelectObjectRequest req = new SelectObjectRequest();
        req.withBucketName(bucket)
                .withKey(key)
                .withExpression("select * from obsobject")
                .withInputSerialization(
                        new InputSerialization()
                                .withCsv(new CsvInput().withFieldDelimiter(',').withFileHeaderInfo(FileHeaderInfo.USE)))
                .withOutputSerialization(
                        new OutputSerialization()
                                .withCsv(new CsvOutput().withFieldDelimiter(',')));

        SelectObjectResult result = client.selectObjectContent(req);
        InputStream is = result.getInputStream();
        InputStreamReader reader = new InputStreamReader(is);
        int charRead = 0;
        while (charRead >= 0) {
            charRead = reader.read();
            System.out.print((char)charRead);
        }
        is.close();
        time = System.currentTimeMillis() - time;
        System.out.println(time);
        String a = " ";
        a.trim();

    }
}
