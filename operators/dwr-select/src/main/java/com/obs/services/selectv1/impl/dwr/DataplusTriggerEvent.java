package com.obs.services.selectv1.impl.dwr;

import com.google.gson.annotations.SerializedName;
import com.huawei.services.runtime.entity.obs.IObsTriggerInfo;
import com.huawei.services.runtime.entity.obs.ObsTriggerEvent;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

public class DataplusTriggerEvent implements IObsTriggerInfo {
	@SerializedName("Records")
	private ObsTriggerEvent.ObsRecord[] record;

	private Map<String, String> inputs;

	private Map<String, Object> dynamic_source;

	private String execution_name;

	private String graph_name;

	private String operation_name;

	public DataplusTriggerEvent() {}

	public DataplusTriggerEvent(ObsTriggerEvent.ObsRecord[] record) {
		this.record = record;
	}

	public ObsTriggerEvent.ObsRecord[] getRecord() {
		return this.record;
	}

	public void setRecord(ObsTriggerEvent.ObsRecord[] record) {
		this.record = record;
	}

	public Map<String, String> getInputs() {
		return this.inputs;
	}

	public void setInputs(Map<String, String> inputs) {
		this.inputs = inputs;
	}

	public Map<String, Object> getDynamic_source() {
		return this.dynamic_source;
	}

	public void setDynamic_source(Map<String, Object> dynamic_source) {
		this.dynamic_source = dynamic_source;
	}

	public String getExecution_name() {
		return this.execution_name;
	}

	public void setExecution_name(String execution_name) {
		this.execution_name = execution_name;
	}

	public String getGraph_name() {
		return this.graph_name;
	}

	public void setGraph_name(String graph_name) {
		this.graph_name = graph_name;
	}

	public String getOperation_name() {
		return this.operation_name;
	}

	public void setOperation_name(String operation_name) {
		this.operation_name = operation_name;
	}

	public String toString() {
		return "DataplusTriggerEvent{record=" +
				Arrays.toString((Object[])this.record) + ", inputs=" + this.inputs + ", execution_name='" + this.execution_name + '\'' + ", graph_name='" + this.graph_name + '\'' + ", operation_name='" + this.operation_name + '\'' + ", dynamic_source=" + this.dynamic_source + '}';
	}

	private Optional<ObsTriggerEvent.ObsRecord[]> check() {
		if (this.record != null && this.record.length >= 1) {
			if (this.record.length > 1)
				throw new IllegalArgumentException("Record's length is to long! ");
			return (Optional)Optional.of(this.record);
		}
		throw new IllegalArgumentException("Record can't be null. ");
	}

	public String getBucketName() {
		Optional<ObsTriggerEvent.ObsRecord[]> obsRecord = check();
		return (String)obsRecord.map(r -> r[0].getObs().getBucket().getName())

				.orElseThrow(IllegalArgumentException::new);
	}

	public String getObjectKey() {
		Optional<ObsTriggerEvent.ObsRecord[]> obsRecord = check();
		return (String)obsRecord.map(r -> r[0].getObs().getObsobject().getKey())

				.orElseThrow(IllegalArgumentException::new);
	}

	public String getEventName() {
		Optional<ObsTriggerEvent.ObsRecord[]> obsRecord = check();
		return (String)obsRecord.map(r -> r[0].getEventName())

				.orElseThrow(IllegalArgumentException::new);
	}

	public class ObsObject {
		private int size;

		private String key;

		private String eTag;

		private String versionId;

		private String sequencer;

		public ObsObject() {}

		public ObsObject(int size, String key, String eTag, String versionId, String sequencer) {
			this.size = size;
			this.key = key;
			this.eTag = eTag;
			this.versionId = versionId;
			this.sequencer = sequencer;
		}

		public int getSize() {
			return this.size;
		}

		public void setSize(int size) {
			this.size = size;
		}

		public String getKey() {
			return this.key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String geteTag() {
			return this.eTag;
		}

		public void seteTag(String eTag) {
			this.eTag = eTag;
		}

		public String getVersionId() {
			return this.versionId;
		}

		public void setVersionId(String versionId) {
			this.versionId = versionId;
		}

		public String getSequencer() {
			return this.sequencer;
		}

		public void setSequencer(String sequencer) {
			this.sequencer = sequencer;
		}

		public String toString() {
			return "ObsObject{size=" + this.size + ", key='" + this.key + '\'' + ", eTag='" + this.eTag + '\'' + ", versionId='" + this.versionId + '\'' + ", sequencer='" + this.sequencer + '\'' + '}';
		}
	}

	public class Bucket {
		private String name;

		private String bucket;

		@SerializedName("ownerIdentity")
		private ObsTriggerEvent.IdEntity idEntity;

		public Bucket() {}

		public Bucket(String name, String bucket, ObsTriggerEvent.IdEntity idEntity) {
			this.name = name;
			this.bucket = bucket;
			this.idEntity = idEntity;
		}

		public String getName() {
			return this.name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getBucket() {
			return this.bucket;
		}

		public void setBucket(String bucket) {
			this.bucket = bucket;
		}

		public ObsTriggerEvent.IdEntity getIdEntity() {
			return this.idEntity;
		}

		public void setIdEntity(ObsTriggerEvent.IdEntity idEntity) {
			this.idEntity = idEntity;
		}

		public String toString() {
			return "Bucket{name='" + this.name + '\'' + ", bucket='" + this.bucket + '\'' + ", ownerIdentity=" + this.idEntity + '}';
		}
	}

	public class IdEntity {
		@SerializedName("ID")
		private String id;

		public IdEntity() {}

		public IdEntity(String id) {
			this.id = id;
		}

		public String getId() {
			return this.id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String toString() {
			return "IdEntity{ID='" + this.id + '\'' + '}';
		}
	}

	public class ObsBody {
		@SerializedName("Version")
		private String version;

		private String configurationId;

		private ObsTriggerEvent.Bucket bucket;

		@SerializedName("object")
		private ObsTriggerEvent.ObsObject obsobject;

		public ObsBody() {}

		public ObsBody(String version, String configurationId, ObsTriggerEvent.Bucket bucket, ObsTriggerEvent.ObsObject obsobject) {
			this.version = version;
			this.configurationId = configurationId;
			this.bucket = bucket;
			this.obsobject = obsobject;
		}

		public String getVersion() {
			return this.version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getConfigurationId() {
			return this.configurationId;
		}

		public void setConfigurationId(String configurationId) {
			this.configurationId = configurationId;
		}

		public ObsTriggerEvent.Bucket getBucket() {
			return this.bucket;
		}

		public void setBucket(ObsTriggerEvent.Bucket bucket) {
			this.bucket = bucket;
		}

		public ObsTriggerEvent.ObsObject getObsobject() {
			return this.obsobject;
		}

		public void setObsobject(ObsTriggerEvent.ObsObject obsobject) {
			this.obsobject = obsobject;
		}

		public String toString() {
			return "ObsBody{Version='" + this.version + '\'' + ", configurationId='" + this.configurationId + '\'' + ", bucket=" + this.bucket + ", object=" + this.obsobject + '}';
		}
	}

	public class RequestParameters {
		private String sourceIPAddress;

		public RequestParameters() {}

		public RequestParameters(String sourceIPAddress) {
			this.sourceIPAddress = sourceIPAddress;
		}

		public String getSourceIPAddress() {
			return this.sourceIPAddress;
		}

		public void setSourceIPAddress(String sourceIPAddress) {
			this.sourceIPAddress = sourceIPAddress;
		}

		public String toString() {
			return "RequestParameters{sourceIPAddress='" + this.sourceIPAddress + '\'' + '}';
		}
	}

	public class ObsRecord {
		private String eventVersion;

		private String eventSource;

		private String eventRegion;

		private String eventTime;

		private String eventName;

		private ObsTriggerEvent.IdEntity userIdentity;

		private ObsTriggerEvent.RequestParameters requestParameters;

		private Map<String, Object> responseElements;

		private ObsTriggerEvent.ObsBody obs;

		public ObsRecord() {}

		public ObsRecord(String eventVersion, String eventSource, String eventRegion, String eventTime, String eventName, ObsTriggerEvent.IdEntity userIdentity, ObsTriggerEvent.RequestParameters requestParameters, Map<String, Object> responseElements, ObsTriggerEvent.ObsBody obs) {
			this.eventVersion = eventVersion;
			this.eventSource = eventSource;
			this.eventRegion = eventRegion;
			this.eventTime = eventTime;
			this.eventName = eventName;
			this.userIdentity = userIdentity;
			this.requestParameters = requestParameters;
			this.responseElements = responseElements;
			this.obs = obs;
		}

		public String getEventVersion() {
			return this.eventVersion;
		}

		public void setEventVersion(String eventVersion) {
			this.eventVersion = eventVersion;
		}

		public String getEventSource() {
			return this.eventSource;
		}

		public void setEventSource(String eventSource) {
			this.eventSource = eventSource;
		}

		public String getEventRegion() {
			return this.eventRegion;
		}

		public void setEventRegion(String eventRegion) {
			this.eventRegion = eventRegion;
		}

		public String getEventTime() {
			return this.eventTime;
		}

		public void setEventTime(String eventTime) {
			this.eventTime = eventTime;
		}

		public String getEventName() {
			return this.eventName;
		}

		public void setEventName(String eventName) {
			this.eventName = eventName;
		}

		public ObsTriggerEvent.IdEntity getUserIdentity() {
			return this.userIdentity;
		}

		public void setUserIdentity(ObsTriggerEvent.IdEntity userIdentity) {
			this.userIdentity = userIdentity;
		}

		public ObsTriggerEvent.RequestParameters getRequestParameters() {
			return this.requestParameters;
		}

		public void setRequestParameters(ObsTriggerEvent.RequestParameters requestParameters) {
			this.requestParameters = requestParameters;
		}

		public Map<String, Object> getResponseElements() {
			return this.responseElements;
		}

		public void setResponseElements(Map<String, Object> responseElements) {
			this.responseElements = responseElements;
		}

		public ObsTriggerEvent.ObsBody getObs() {
			return this.obs;
		}

		public void setObs(ObsTriggerEvent.ObsBody obs) {
			this.obs = obs;
		}

		public String toString() {
			return "ObsRecord{eventVersion='" + this.eventVersion + '\'' + ", eventSource='" + this.eventSource + '\'' + ", eventRegion='" + this.eventRegion + '\'' + ", eventTime='" + this.eventTime + '\'' + ", eventName='" + this.eventName + '\'' + ", userIdentity=" + this.userIdentity + ", requestParameters=" + this.requestParameters + ", responseElements=" + this.responseElements + ", obs=" + this.obs + '}';
		}
	}
}
