package com.obs.services.selectv1.impl.dwr;

import com.obs.services.selectv1.FileHeaderInfo;

public class DwrRequestParam {
    public String expression;
    public String iFieldDelimiter;
    public String iRecordDelimiter;
    public String oFieldDelimiter;
    public String oRecordDelimiter;
    public FileHeaderInfo fileHeaderInfo;
    public String dstBucket;
    public String dstDir;
    public DwrResponseMode dwrResponseMode;

    public static enum DwrResponseMode {
        JSON("json"),
        FILE_STREAM("file_stream");
        private final String mode;

        DwrResponseMode(String mode) {
            this.mode = mode;
        }
        public String toString() {
            return this.mode;
        }
    }
}
