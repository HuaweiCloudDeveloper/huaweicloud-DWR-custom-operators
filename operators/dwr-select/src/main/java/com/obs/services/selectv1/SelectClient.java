package com.obs.services.selectv1;

import com.obs.services.ObsClient;
import com.obs.services.ObsConfiguration;
import com.obs.services.selectv1.impl.CsvJdbcSelect;
import com.obs.services.selectv1.impl.DwrSelect;
import com.obs.services.selectv1.impl.ISelect;
import com.obs.services.exception.ObsException;
import com.obs.services.selectv1.impl.dwr.DwrRequestParam;
import okhttp3.OkHttpClient;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class SelectClient {
    private final SelectConfig conf;
    private ISelect iSelect;
    private ObsClient obsClient;
    private OkHttpClient okHttpClient;

    public SelectClient(SelectConfig conf) {
        this.conf = conf;
        conf.verify();
        initClients(conf);
        if (conf.mode == SelectMode.LOCAL) {
            initLocalMode(conf);
        }
        if (conf.mode == SelectMode.DWR) {
            initDwrMode(conf);
        }
    }

    private void initClients(SelectConfig conf) {
        String ak = conf.ak;
        String sk = conf.sk;
        String endpoint = conf.endpoint;
        int timeout = conf.timeout;

        // init obsClient
        ObsConfiguration obsConf = new ObsConfiguration();
        obsConf.setConnectionTimeout(timeout * 1000);
        obsConf.setSocketTimeout(timeout * 1000);
        obsConf.setEndPoint(endpoint);
        this.obsClient = new ObsClient(ak, sk, obsConf);

        // init okhttpClient
        this.okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .writeTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .build();
    }

    private void initLocalMode(SelectConfig conf) {
        iSelect = new CsvJdbcSelect(conf.ak, conf.sk, conf.endpoint, conf.timeout);
    }

    private void initDwrMode(SelectConfig conf) {
//        iSelect = new DwrSelect(conf.ak, conf.sk, conf.endpoint, conf.dwrWorkflowName, conf.dwrDstBucket, conf.dwrDstDir);
        iSelect = new DwrSelect(conf.ak, conf.sk, conf.endpoint, conf.dwrWorkflowName, conf.dwrDstBucket,
                conf.dwrDstDir, conf.timeout, conf.dwrResponseMode, this.obsClient, this.okHttpClient);
    }

    public SelectObjectResult selectObjectContent(final SelectObjectRequest selectRequest) throws ObsException {
        String bucket = selectRequest.getBucketName();
        String key = selectRequest.getKey();
        String expression = selectRequest.getExpression();
        Character iFieldDelimiter = selectRequest.getInputSerialization().getInput().getFieldDelimiter();
//        Character iRecordDelimiter = selectRequest.getInputSerialization().getInput().getRecordDelimiter();
        Character iRecordDelimiter = '\n';
        FileHeaderInfo iFileHeaderInfo = selectRequest.getInputSerialization().getInput().getFileHeaderInfo();
        Character oFieldDelimiter = selectRequest.getOutputSerialization().getOutput().getFieldDelimiter();
//        Character oRecordDelimiter = selectRequest.getOutputSerialization().getOutput().getRecordDelimiter();
        Character oRecordDelimiter = '\n';

        try {
            InputStream ret = iSelect.select(
                    bucket,
                    key,
                    expression,
                    iFieldDelimiter.toString(),
                    iRecordDelimiter.toString(),
                    iFileHeaderInfo,
                    oFieldDelimiter.toString(),
                    oRecordDelimiter.toString());
            return new SelectObjectResult(ret);
        } catch (SQLException sqlException) {
            throw new ObsException("selectObject exception in " + conf.mode + " mode", sqlException);
        }
    }

    public static class SelectConfig {
        public SelectMode mode = SelectMode.LOCAL;
        public String ak;
        public String sk;
        public String endpoint;
        public String dwrWorkflowName;
        public String dwrDstBucket;
        public String dwrDstDir;
        public int timeout = 30;
        public DwrRequestParam.DwrResponseMode dwrResponseMode = DwrRequestParam.DwrResponseMode.JSON;

        void verify() {
            if (mode == SelectMode.LOCAL && (
                    ak == null || ak.isEmpty() ||
                    sk == null || sk.isEmpty() ||
                    endpoint == null || endpoint.isEmpty())) {
                throw new IllegalArgumentException("local mode should set 'ak', 'sk', 'endpoint' in config");
            }

            if (mode == SelectMode.DWR && (
                    ak == null || ak.isEmpty() ||
                            sk == null || sk.isEmpty() ||
                            endpoint == null || endpoint.isEmpty() ||
                            dwrWorkflowName == null || dwrWorkflowName.isEmpty())) {
                throw new IllegalArgumentException("dwr mode should set 'ak', 'sk', 'endpoint', 'dwrWorkflowName' in config");
            }
        }
    }

    public enum SelectMode {
        LOCAL("local"),
        DWR("dwr");

        private final String mode;

        SelectMode(String mode) {
            this.mode = mode;
        }

        public String toString() {
            return this.mode;
        }

    }
}
