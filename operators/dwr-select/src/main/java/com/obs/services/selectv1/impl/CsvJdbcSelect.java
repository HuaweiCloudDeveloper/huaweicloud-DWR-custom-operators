package com.obs.services.selectv1.impl;

import com.obs.services.ObsClient;
import com.obs.services.model.GetObjectRequest;
import com.obs.services.model.ObsObject;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import com.obs.services.selectv1.FileHeaderInfo;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.sql.*;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingDeque;

public class CsvJdbcSelect implements ISelect {
    private final String ak;
    private final String sk;
    private final String endpoint;
    private int timeout = 30;

    public CsvJdbcSelect(String ak, String sk, String endpoint) {
        this.ak = ak;
        this.sk = sk;
        this.endpoint = endpoint;
    }

    public CsvJdbcSelect(String ak, String sk, String endpoint,
                         int timeout) {
        this.ak = ak;
        this.sk = sk;
        this.endpoint = endpoint;
        this.timeout = timeout;
    }

    @Override
    public InputStream select(
            String bucket,
            String key,
            String expression,
            String iFieldDelimiter,
            String iRecordDelimiter,
            FileHeaderInfo fileHeaderInfo,
            String oFieldDelimiter,
            String oRecordDelimiter
    ) throws SQLException {
        String propString = "";
        try {
            propString = String.format("ak=%s&sk=%s&endpoint=%s&bucket=%s&key=%s&timeout=%d",
                    URLEncoder.encode(ak, "UTF-8"),
                    URLEncoder.encode(sk, "UTF-8"),
                    URLEncoder.encode(endpoint, "UTF-8"),
                    URLEncoder.encode(bucket, "UTF-8"),
                    URLEncoder.encode(key, "UTF-8"),
                    timeout);
        } catch (UnsupportedEncodingException e) {
            throw new SQLException(e);
        }
        String url = "jdbc:relique:csv:class:com.obs.services.selectv1.impl.reader.ObsTableReader?" + propString;
        Properties properties = new Properties();
        properties.put("separator", iFieldDelimiter);
        properties.put("quoteStyle", "C");
        String suppressHeaders = "false";
        String skipLeadingLines = "0";
        boolean genHeader = false;
        switch (fileHeaderInfo) {
            case USE:
                suppressHeaders = "false";
                skipLeadingLines = "0";
                break;
            case NONE:
                suppressHeaders = "true";
                skipLeadingLines = "0";
                genHeader = true;
                break;
            case IGNORE:
                suppressHeaders = "true";
                skipLeadingLines = "1";
                genHeader = true;
                break;
        }
        properties.put("suppressHeaders", suppressHeaders);
        properties.put("skipLeadingLines", skipLeadingLines);
        try{
            if (genHeader) {
                int colNum = getColumnNum(bucket, key, CSVFormat.DEFAULT.builder()
                        .setDelimiter(iFieldDelimiter)
                        .setRecordSeparator(iRecordDelimiter)
                        .build());
                if (colNum == 0) {
                    return new InputStream() {
                        @Override
                        public int read() throws IOException {
                            return -1;
                        }
                    };
                }
//                System.out.printf("CsvJdbcSelect colNum: %d\n", colNum);
                String headerString = genHeaderString(colNum);
                properties.put("headerline", headerString);
            }
            Connection conn = DriverManager.getConnection(url, properties);
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(expression);
            CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                    .setDelimiter(oFieldDelimiter)
                    .setRecordSeparator(oRecordDelimiter)
                    .setQuoteMode(QuoteMode.NONE)
                    .setEscape('\\')
                    .build();
            return new CsvJdbcSelectInputStream(conn, resultSet, csvFormat);
        } catch (IOException e) {
            throw new SQLException(e);
        }
    }

    private String genHeaderString(int columnNum) {
        String[] headers = new String[columnNum];
        for (int i = 0; i < columnNum; i ++) {
            headers[i] = "_" + (i + 1);
        }
        return String.join(",", headers);
    }

    private int getColumnNum(String bucket, String key, CSVFormat csvFormat) throws IOException {
        ObsClient obsClient = new ObsClient(ak, sk, endpoint);
        GetObjectRequest req = new GetObjectRequest();
        req.setBucketName(bucket);
        req.setObjectKey(key);
        ObsObject obj = obsClient.getObject(req);
        InputStreamReader reader = new InputStreamReader(obj.getObjectContent());
        Iterator<CSVRecord> iter = csvFormat.parse(reader).stream().iterator();
        if (!iter.hasNext()) {
            return 0;
        }
        CSVRecord record = iter.next();
        int ret = record.size();
        reader.close();
        obsClient.close();
        return ret;
    }

    private static class CsvJdbcSelectInputStream extends InputStream {
        private static final int BufferSize = 4 * 1024;

        private final CSVPrinter csvPrinter;
        private final Connection conn;
        private final ResultSet resultSet;
        private final ResultSetMetaData resultSetMetaData;
        private final LinkedBlockingDeque<ByteBuffer> buffers;

        CsvJdbcSelectInputStream(Connection conn, ResultSet resultSet, CSVFormat csvFormat) throws IOException {
            this.conn = conn;
            this.resultSet = resultSet;
            try {
                this.resultSetMetaData = resultSet.getMetaData();
            } catch (SQLException e) {
                throw new IOException(e);
            }
            this.buffers = new LinkedBlockingDeque<>();
            Appendable appendable = new Appendable() {
                private ByteBuffer getLastBuffer() {
                    ByteBuffer lastBuffer = buffers.peekLast();
                    if (lastBuffer == null) {
                        lastBuffer = ByteBuffer.allocate(BufferSize);
                        buffers.offerLast(lastBuffer);
                    }
                    return lastBuffer;
                }

                private ByteBuffer newBuffer() {
                    ByteBuffer lastBuffer = buffers.peekLast();
                    if (lastBuffer != null) {
                        lastBuffer.flip();
                    }
                    lastBuffer = ByteBuffer.allocate(BufferSize);
                    buffers.offerLast(lastBuffer);
                    return lastBuffer;
                }

                private void fillBuffer(byte[] buf) {
                    ByteBuffer lastBuffer = getLastBuffer();
                    int start = 0;
                    int end = buf.length;
                    while(true) {
                        if (lastBuffer.remaining() >= end - start) {
                            lastBuffer.put(buf, start, end - start);
                            break;
                        } else {
                            int remain = lastBuffer.remaining();
                            lastBuffer.put(buf, start, remain);
                            start += remain;
                            lastBuffer = newBuffer();
                        }
                    }
                }

                @Override
                public Appendable append(CharSequence csq) throws IOException {
                    if (csq == null) {
                        return append(csq, 0, 0);
                    } else {
                        return append(csq, 0, csq.length());
                    }
                }

                @Override
                public Appendable append(CharSequence csq, int start, int end) throws IOException {
                    byte[] buf = csq.subSequence(start, end).toString().getBytes();
                    fillBuffer(buf);
                    return this;
                }

                @Override
                public Appendable append(char c) throws IOException {
                    return append(String.valueOf(c));
                }
            };
            this.csvPrinter = new CSVPrinter(appendable, csvFormat);
        }

        private ByteBuffer getFirstBufferToRead() {
            ByteBuffer first = buffers.peekFirst();
            if (first == null) {
                return null;
            }
            if (!first.hasRemaining()) {
                buffers.pollFirst();
                first = buffers.peekFirst();
            }
            if (first == null) {
                return null;
            }
            if (!first.hasRemaining()) {
                throw new IllegalStateException("should not get empty buffer in this place");
            }
            return first;
        }

        private void fetchData() throws IOException {
            try {
                Object[] vals = new Object[resultSetMetaData.getColumnCount()];
                while (resultSet.next()) {
                    for (int i = 0; i < resultSetMetaData.getColumnCount(); i++) {
                        vals[i] = resultSet.getObject(i + 1);
                    }
                    csvPrinter.printRecord(vals);
                    csvPrinter.flush();
                    if (buffers.size() >= 4) {
                        break;
                    }
                }
                ByteBuffer lastBuffer = buffers.peekLast();
                if (lastBuffer != null) {
                    lastBuffer.flip();
                }
            } catch (SQLException e) {
                throw new IOException(e);
            }
        }

        private ByteBuffer ensureData() throws IOException {
            ByteBuffer first = getFirstBufferToRead();
            if (first != null) {
                return first;
            }
            // try to load data
            fetchData();
            return getFirstBufferToRead();
        }

        @Override
        public int read() throws IOException {
            ByteBuffer first = ensureData();
            if (first == null) {
                return -1;
            }
            return first.get() & 0xFF;
        }

        @Override
        public int read(byte[] buf, int start, int len) throws IOException {
            ByteBuffer first = ensureData();
            if (first == null) {
                return -1;
            }
            int bytesRead = Math.min(len, first.remaining());
            first.get(buf, start, bytesRead);
            return bytesRead;
        }

        @Override
        public void close() throws IOException {
            try {
                resultSet.close();
                conn.close();
            } catch (SQLException e) {
                throw new IOException(e);
            }
            csvPrinter.close();
            super.close();
        }
    }
}
