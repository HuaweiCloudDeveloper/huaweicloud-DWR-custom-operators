package com.obs.services.selectv1.impl.dwr;

public class DwrJsonResponse {
    public String resultBucket;
    public String resultObjKey;
    public Boolean hasError;
    public String errorString;
}
