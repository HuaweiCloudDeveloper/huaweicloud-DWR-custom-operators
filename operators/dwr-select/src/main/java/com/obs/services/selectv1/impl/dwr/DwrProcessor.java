package com.obs.services.selectv1.impl.dwr;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.huawei.services.runtime.Context;
import com.obs.services.ObsConfiguration;
import com.obs.services.model.PutObjectRequest;
import com.obs.services.selectv1.impl.CsvJdbcSelect;
import com.obs.services.selectv1.impl.ISelect;
import com.obs.services.ObsClient;
import com.obs.services.selectv1.FileHeaderInfo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class DwrProcessor {

    public DataplusTriggerEvent handler(DataplusTriggerEvent event, Context context) throws Exception {
        String ak = context.getAccessKey();
        String sk = context.getSecretKey();
        String regionId = context.getUserData("region");
        String clientTimeoutStr = context.getUserData("clientTimeout");
        String dstBucket = context.getUserData("dstBucket"); // 从环境变量获取dstBucket
        String dstDir = context.getUserData("dstDir");
        String dstExpire = context.getUserData("dstExpire");
        int clientTimeout = 30;
        if (!stringEmpty(clientTimeoutStr)) {
            clientTimeout = Integer.parseInt(clientTimeoutStr);
        }
        int expire = 1;
        try {
            expire = Integer.parseInt(dstExpire);
        } catch (Exception e) {
            // do nothing
        }
        String endpoint = "";
        if (regionId.equals("cn-north-7")) {
            endpoint = "obs.cn-north-7.ulanqab.huawei.com";
        } else {
            endpoint = "obs." + regionId + ".myhuaweicloud.com";
        }
        ISelect iSelect = new CsvJdbcSelect(ak, sk, endpoint);

        FssParam param = collectParam(event);
        if (!stringEmpty(param.dstBucket)) { // 客户端传入的dstBucket优先与环境变量
            dstBucket = param.dstBucket;
        }
        if (!stringEmpty(param.dstDir)) {
            dstDir = param.dstDir;
        }
        if (stringEmpty(dstBucket)) { // 若环境变量未配置 且 客户端未传入， 则使用业务桶
            dstBucket = param.bucket;
        }

        InputStream in = iSelect.select(
                param.bucket,
                param.key,
                param.expression,
                param.iFieldDelimiter,
                param.iRecordDelimiter,
                param.fileHeaderInfo,
                param.oFieldDelimiter,
                param.oRecordDelimiter
        );
        in = new SequenceInputStream(in, new ByteArrayInputStream(new byte[]{' '})); // 为了规避：dwr在返回空对象时报503错误

        if (stringEmpty(dstBucket) || stringEmpty(dstDir)) {
            throw new IllegalArgumentException("need dstBucket and dstDir params");
        }

        if (!dstDir.endsWith("/")) {
            dstDir = dstDir + "/";
        }

        String resDir = dstDir + param.bucket + "/"; // 源桶名要体现在路径中
        String resKey = param.key + "." + System.currentTimeMillis() + ".csv";

        ObsClient client = getObsClient(ak, sk, endpoint, clientTimeout * 1000);
        PutObjectRequest req = new PutObjectRequest();
        req.setBucketName(dstBucket);
        req.setObjectKey(resDir + resKey);
        req.setInput(in);
        req.setExpires(expire);
        client.putObject(dstBucket, resDir + resKey, in);

        // close resource
        in.close(); // putObject是否会关闭？
        client.close();

        Object out = null;

        if (param.dwrResponseMode == DwrRequestParam.DwrResponseMode.FILE_STREAM) {
            FssOutput fssOut = new FssOutput();
            fssOut.bucket = dstBucket;
            fssOut.dir = resDir;
            fssOut.region = regionId;
            fssOut.key = resKey;
            out = fssOut;
        }

        if (param.dwrResponseMode == DwrRequestParam.DwrResponseMode.JSON) {
            DwrJsonResponse jsonOut = new DwrJsonResponse();
            jsonOut.hasError = false;
            jsonOut.resultBucket = dstBucket;
            jsonOut.resultObjKey = resDir + resKey;
            out = jsonOut;
        }

        if (out == null) {
            throw new IllegalStateException("invalid dwrResponseMode: " + param.dwrResponseMode);
        }

        Map<String, Object> dynamicSource = new HashMap<>();
        Map<String, Object>[] tasks = new HashMap[1];
        Map<String, Object> res = new HashMap<>();
        res.put("output", out);
        tasks[0] = res;
        dynamicSource.put("tasks", tasks);
        event.setDynamic_source(dynamicSource);
        return event;
    }

    private FssParam collectParam(DataplusTriggerEvent event) throws IOException {
        FssParam ret = new FssParam();
        ret.bucket = event.getBucketName();
        ret.key = event.getObjectKey();
        if (stringEmpty(ret.bucket) || stringEmpty(ret.key)) {
            throw new IOException("bucket or object key can not be empty");
        }

        Map<String, Object> dynamicSource = event.getDynamic_source();
        String paramsJson = decodeParam(dynamicSource.get("params"));
        Gson gson = new Gson();
        DwrRequestParam params = gson.fromJson(paramsJson, DwrRequestParam.class);

        ret.expression = params.expression;
        ret.iFieldDelimiter = params.iFieldDelimiter;
        ret.iRecordDelimiter = params.iRecordDelimiter;
        ret.fileHeaderInfo = params.fileHeaderInfo;
        ret.oFieldDelimiter = params.oFieldDelimiter;
        ret.oRecordDelimiter = params.oRecordDelimiter;
        ret.dstBucket = params.dstBucket;
        ret.dstDir = params.dstDir;
        ret.dwrResponseMode = params.dwrResponseMode;
        if (ret.dwrResponseMode == null) {
            throw new IOException("get null dwrResponseMode");
        }

        return ret;
    }

    private String decodeParam(Object obj) {
        return new String(Base64.getUrlDecoder().decode(obj.toString()), StandardCharsets.UTF_8);
    }

    private boolean stringEmpty(String str) {
        return str == null || str.equals("");
    }

    private ObsClient getObsClient(String ak, String sk, String endpoint, int timeout) {
        ObsConfiguration obsConfiguration = new ObsConfiguration();
        obsConfiguration.setEndPoint(endpoint);
        obsConfiguration.setConnectionTimeout(timeout);
        obsConfiguration.setSocketTimeout(timeout);
        return new ObsClient(ak, sk, obsConfiguration);
    }

    private static class FssParam {
        String bucket;
        String key;
        String expression;
        String iFieldDelimiter;
        String iRecordDelimiter;
        FileHeaderInfo fileHeaderInfo;
        String oFieldDelimiter;
        String oRecordDelimiter;
        String dstBucket;
        String dstDir;
        DwrRequestParam.DwrResponseMode dwrResponseMode;
    }

    // 文档：
    private static class FssOutput {
        @SerializedName("bucket")
        String bucket;

        @SerializedName("object")
        String dir;

        @SerializedName("file_name")
        String key;

        @SerializedName("location")
        String region;
    }
}
