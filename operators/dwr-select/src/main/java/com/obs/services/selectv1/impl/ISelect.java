package com.obs.services.selectv1.impl;

import com.obs.services.selectv1.FileHeaderInfo;

import java.io.InputStream;
import java.sql.SQLException;

public interface ISelect {
    InputStream select(
            String bucket,
            String key,
            String expression,
            String iFieldDelimiter,
            String iRecordDelimiter,
            FileHeaderInfo fileHeaderInfo,
            String oFieldDelimiter,
            String oRecordDelimiter
    ) throws SQLException;
}
