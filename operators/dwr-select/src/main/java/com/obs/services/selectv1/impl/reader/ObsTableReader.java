package com.obs.services.selectv1.impl.reader;

import com.obs.services.ObsClient;
import com.obs.services.ObsConfiguration;
import com.obs.services.model.ObsObject;
import org.relique.io.TableReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObsTableReader implements TableReader {

    public static String PROP_AK = "ak";
    public static String PROP_SK = "sk";
    public static String PROP_ENDPOINT = "endpoint";
    public static String PROP_BUCKET = "bucket";
    public static String PROP_KEY = "key";
    public static String PROP_TIMEOUT = "timeout";
    private static ObsClient obsClient = null;

    @Override
    public Reader getReader(Statement statement, String table) throws SQLException {
        String properties = getConnUrlProperties(statement.getConnection());
        Map<String, String> prop = parseUrlProperties(properties);
        String key = prop.get(PROP_KEY);
        String bucket = prop.get(PROP_BUCKET);
        ObsClient obsClient = getObsClient(prop);
        ObsObject obj = obsClient.getObject(bucket, key);
        InputStream inputStream = obj.getObjectContent();
        return new InputStreamReader(inputStream);
    }

    @Override
    public List<String> getTableNames(Connection connection) {
        throw new UnsupportedOperationException("unsupport getTableNames");
    }

    private ObsClient getObsClient(Map<String, String> prop) {
        if (obsClient == null) {
            synchronized (ObsClient.class) {
                if (obsClient == null) {
                    obsClient = innerGetObsClient(prop);
                }
            }
        }
        return obsClient;
    }

    private ObsClient innerGetObsClient(Map<String, String> prop) {
        String ak = prop.get(PROP_AK);
        String sk = prop.get(PROP_SK);
        String endpoint = prop.get(PROP_ENDPOINT);
        String timeoutStr = prop.get(PROP_TIMEOUT);
        ObsConfiguration conf = new ObsConfiguration();
        conf.setEndPoint(endpoint);
        try {
            int timeout = Integer.parseInt(timeoutStr) * 1000;
            conf.setConnectionTimeout(timeout);
            conf.setSocketTimeout(timeout);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            // do nothing
        }
        return new ObsClient(ak, sk, conf);
    }

    private String uniformPrefix(String prefix) {
        if (prefix.endsWith("/")) {
            return prefix;
        }
        return prefix + "/";
    }

    private String getConnUrlProperties(Connection conn) throws SQLException {
        String url = conn.getMetaData().getURL();
        int questionIndex = url.indexOf('?');
        if (questionIndex >= 0) {
            return url.substring(questionIndex);
        }
        return "";
    }

    private Map<String, String> parseUrlProperties(String urlProperties) {
        Map<String, String> ret = new HashMap<>();
        String[] split = urlProperties.substring(1).split("&");
        for (int i = 0; i < split.length; i++)
        {
            int equalsIndex = split[i].indexOf("=");
            if (equalsIndex <= 0)
                throw new IllegalArgumentException("invalidProperty" + ": " + split[i]);
            int lastEqualsIndex = split[i].lastIndexOf("=");
            if (lastEqualsIndex != equalsIndex)
                throw new IllegalArgumentException("invalidProperty" + ": " + split[i]);

            try
            {
                String key = URLDecoder.decode(split[i].substring(0, equalsIndex), "UTF-8");
                String value = URLDecoder.decode(split[i].substring(equalsIndex + 1), "UTF-8");
                ret.put(key, value);
            }
            catch (UnsupportedEncodingException e)
            {
                // we know UTF-8 is available
            }
        }
        return ret;
    }
}
