package com.obs.services.selectv1.impl;

import com.google.gson.Gson;
import com.obs.services.ObsClient;
import com.obs.services.model.ObsObject;
import com.obs.services.selectv1.FileHeaderInfo;
import com.obs.services.selectv1.impl.dwr.DwrJsonResponse;
import com.obs.services.selectv1.impl.dwr.DwrRequestParam;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class DwrSelect implements ISelect {

    private final String ak;
    private final String sk;
    private final String endpoint;
    private final String workflowName;
    private String dstBucket;
    private String dstDir;
    private int timeout = 30;
    private DwrRequestParam.DwrResponseMode dwrResponseMode = DwrRequestParam.DwrResponseMode.JSON;
    private ObsClient obsClient;
    private OkHttpClient okHttpClient;

    public DwrSelect(String ak, String sk, String endpoint, String workflowName) {
        this.ak = ak;
        this.sk = sk;
        this.endpoint = endpoint;
        this.workflowName = workflowName;
    }

    public DwrSelect(String ak, String sk, String endpoint, String workflowName, String dstBucket, String dstDir) {
        this.ak = ak;
        this.sk = sk;
        this.endpoint = endpoint;
        this.workflowName = workflowName;
        this.dstBucket = dstBucket;
        this.dstDir = dstDir;
    }

    public DwrSelect(
            String ak,
            String sk,
            String endpoint,
            String workflowName,
            String dstBucket,
            String dstDir,
            int timeout,
            DwrRequestParam.DwrResponseMode dwrResponseMode,
            ObsClient obsClient,
            OkHttpClient okHttpClient) {
        this.ak = ak;
        this.sk = sk;
        this.endpoint = endpoint;
        this.workflowName = workflowName;
        this.dstBucket = dstBucket;
        this.dstDir = dstDir;
        this.timeout = timeout;
        this.dwrResponseMode = dwrResponseMode;
        this.obsClient = obsClient;
        this.okHttpClient = okHttpClient;
    }

    // 签名文档：https://support.huaweicloud.com/usermanual-dwr/dwr_03_0010.html
    private String getSignature(String stringToSign) throws Exception {
        SecretKeySpec signingKey = new SecretKeySpec(this.sk.getBytes("utf-8"), "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signingKey);
        return Base64.getEncoder().encodeToString(mac.doFinal(stringToSign.getBytes("utf-8")));
    }

    @Override
    public InputStream select(
            String bucket,
            String key,
            String expression,
            String iFieldDelimiter,
            String iRecordDelimiter,
            FileHeaderInfo fileHeaderInfo,
            String oFieldDelimiter,
            String oRecordDelimiter) throws SQLException {

        String url = "https://" + bucket + "." + endpoint;
        String signUrl = "/" + key + "?x-workflow-graph-name=" + workflowName;
        DwrRequestParam params = new DwrRequestParam();
        params.expression = expression;
        params.iFieldDelimiter = iFieldDelimiter;
        params.iRecordDelimiter = iRecordDelimiter;
        params.oFieldDelimiter = oFieldDelimiter;
        params.oRecordDelimiter = oRecordDelimiter;
        params.fileHeaderInfo = fileHeaderInfo;
        params.dstBucket = dstBucket;
        params.dstDir = dstDir;
        params.dwrResponseMode = dwrResponseMode;

        Gson gson = new Gson();
        String paramsJson = gson.toJson(params);
        String paramsBase64 = Base64.getEncoder().encodeToString(paramsJson.getBytes(StandardCharsets.UTF_8));

        List<String> queries = new ArrayList<>();
        queries.add("params_" + paramsBase64);
        signUrl += "/" + String.join(",", queries);
        try {
            Date now = Calendar.getInstance().getTime();
            String stringToSign = "GET\n\n\n\nx-obs-date:" + now.toGMTString() + "\n/" + bucket + signUrl;
            String signature = getSignature(stringToSign);

            Request.Builder reqBuilder = new Request.Builder();
            reqBuilder.addHeader("Authorization", "OBS " + ak + ":" + signature);
            reqBuilder.addHeader("X-OBS-Date", now.toGMTString());
            Request req = reqBuilder.get().url(url + signUrl).build();

            Response response = okHttpClient.newCall(req).execute();
            if (dwrResponseMode == DwrRequestParam.DwrResponseMode.FILE_STREAM) {
                return handleFileStreamResp(response);
            }
            if (dwrResponseMode == DwrRequestParam.DwrResponseMode.JSON) {
                return handleJsonResp(response, gson);
            }
        } catch (Exception e) {
            throw new SQLException(e);
        }
        throw new IllegalStateException("invalid dwrResponseMode");
    }

    private InputStream handleFileStreamResp(Response response) throws IOException {
        if (response.code() < 200 || response.code() >= 300) {
            // err state
            byte[] retBody = response.body().bytes();
            String errMsg = String.format("server side error: %s, statusCode = %d", new String(retBody), response.code());
            throw new IOException(errMsg);
        }
        return response.body().byteStream();
    }

    private InputStream handleJsonResp(Response response, Gson gson) throws IOException {
        if (response.code() < 200 || response.code() >= 300) {
            // err state
            byte[] retBody = response.body().bytes();
            String errMsg = String.format("server side error: %s, statusCode = %d", new String(retBody), response.code());
            throw new IOException(errMsg);
        }
        String rspStr = new String(response.body().bytes());
        System.out.println(rspStr);
//        Map<String, DwrJsonResponse> jsonResponse = gson.fromJson(new InputStreamReader(response.body().byteStream()), HashMap.class);
        DwrJsonOutput dwrOutput = gson.fromJson(rspStr, DwrJsonOutput.class);
        DwrJsonResponse jsonResponse = dwrOutput.output;
        if (jsonResponse.hasError) {
            throw new IOException(jsonResponse.errorString);
        }
        ObsObject obsObject = obsClient.getObject(jsonResponse.resultBucket, jsonResponse.resultObjKey);
        return obsObject.getObjectContent();
    }

    private static class DwrJsonOutput {
        DwrJsonResponse output;
    }
}
